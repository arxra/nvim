# Editor Config

While there has been a surge in VSCode in the last few years,
it never quite fit my purposes when I needed to write C back in 2016.
Searching around and being a fan of the terminal landed me on vim and later, neovim.
This is my configuration files for that environment.

## Screenshots

![Some Scala](./pictures/some_scala.png)
![Telescope is really cool for finding stuff, but slow in larger projects(for now)](./pictures/telescope.png)

## Requirements

- neovim nightly
- Lua

## Targets

At university, we use a plethora of different languages which my editor needs to handle well,
preferably with language servers:

- C/C++
- Java
- Rust
- Python
- Scala
- Assembler (no language server setup yet)
and probably more.

At work, there's also JS/TS to worry about (unfortunatly).

## theme

What I think is pleasant to work in changes as often as the languages I work in, so there is no consistency between them.

## Plugins

One plugin which have stuck is coc, which works really well.
Everything else in the setupt will change without notice.

## environment

Alacritty is the only terminal that is fast enough on a old macbook (2016) whith a virtual machine in the background and Cypress tests in the foreground. Strongly recommended.
