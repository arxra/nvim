" Automatically generated packer.nvim plugin loader code

if !has('nvim-0.5')
  echohl WarningMsg
  echom "Invalid Neovim version for packer.nvim!"
  echohl None
  finish
endif

packadd packer.nvim

try

lua << END
local package_path_str = "/home/jarhb/.cache/nvim/packer_hererocks/2.0.5/share/lua/5.1/?.lua;/home/jarhb/.cache/nvim/packer_hererocks/2.0.5/share/lua/5.1/?/init.lua;/home/jarhb/.cache/nvim/packer_hererocks/2.0.5/lib/luarocks/rocks-5.1/?.lua;/home/jarhb/.cache/nvim/packer_hererocks/2.0.5/lib/luarocks/rocks-5.1/?/init.lua"
local install_cpath_pattern = "/home/jarhb/.cache/nvim/packer_hererocks/2.0.5/lib/lua/5.1/?.so"
if not string.find(package.path, package_path_str, 1, true) then
  package.path = package.path .. ';' .. package_path_str
end

if not string.find(package.cpath, install_cpath_pattern, 1, true) then
  package.cpath = package.cpath .. ';' .. install_cpath_pattern
end

local function try_loadstring(s, component, name)
  local success, result = pcall(loadstring(s))
  if not success then
    print('Error running ' .. component .. ' for ' .. name)
    error(result)
  end
  return result
end

_G.packer_plugins = {
  ["braceless.vim"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/braceless.vim"
  },
  ["coc.nvim"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/coc.nvim"
  },
  edge = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/edge"
  },
  ["galaxyline.nvim"] = {
    config = { "\27LJ\1\0023\0\0\2\0\2\0\0044\0\0\0%\1\1\0>\0\2\1G\0\1\0\24statusline/evilline\frequire\0" },
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/galaxyline.nvim"
  },
  ["git-messenger.vim"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/git-messenger.vim"
  },
  ["nvim-tree.lua"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/nvim-tree.lua"
  },
  ["nvim-treesitter"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/nvim-treesitter"
  },
  ["nvim-web-devicons"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/nvim-web-devicons"
  },
  ["oceanic-material"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/oceanic-material"
  },
  ["oceanic-next"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/oceanic-next"
  },
  ["packer.nvim"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/packer.nvim"
  },
  ["plenary.nvim"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/plenary.nvim"
  },
  ["popup.nvim"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/popup.nvim"
  },
  ["telescope.nvim"] = {
    config = {
      set_env = {
        COLORTERM = "truecolor"
      },
      vimgrep_arguments = { "rg", "--color=never", "--no-heading", "--with-filename", "--line-number", "--column", "--smart-case" }
    },
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/telescope.nvim"
  },
  undotree = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/undotree"
  },
  ["vim-dadbod"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-dadbod"
  },
  ["vim-dadbod-ui"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-dadbod-ui"
  },
  ["vim-dispatch"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-dispatch"
  },
  ["vim-doge"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-doge"
  },
  ["vim-fugitive"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-fugitive"
  },
  ["vim-gitgutter"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-gitgutter"
  },
  ["vim-prolog"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-prolog"
  },
  ["vim-repeat"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-repeat"
  },
  ["vim-snippets"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-snippets"
  },
  ["vim-speeddating"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-speeddating"
  },
  ["vim-surround"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-surround"
  },
  ["vim-test"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-test"
  },
  ["vim-toml"] = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vim-toml"
  },
  vimspector = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vimspector"
  },
  vimwiki = {
    loaded = true,
    path = "/home/jarhb/.local/share/nvim/site/pack/packer/start/vimwiki"
  }
}

-- Config for: galaxyline.nvim
try_loadstring("\27LJ\1\0023\0\0\2\0\2\0\0044\0\0\0%\1\1\0>\0\2\1G\0\1\0\24statusline/evilline\frequire\0", "config", "galaxyline.nvim")
-- Config for: telescope.nvim
END

catch
  echohl ErrorMsg
  echom "Error in packer_compiled: " .. v:exception
  echom "Please check your config for correctness"
  echohl None
endtry
