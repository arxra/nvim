" Load all plugins(and the plugin loader)
lua <<eof
  local execute = vim.api.nvim_command
  local fn = vim.fn
  
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  
  if fn.empty(fn.glob(install_path)) > 0 then
    execute('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
    execute 'packadd packer.nvim'
  end

  require('plugins')
eof
autocmd BufWritePost plugins.lua PackerCompile

" === Persistent Undo and buffer changes  ============ {{{

" Keep undo history across sessions, by storing in file.
silent !mkdir ~/.config/nvim/backups > /dev/null 2>&1
set undodir=~/.config/nvim/backups
set undofile
set autowrite

" ================ Indentation and folding ============ {{{
set norelativenumber
set number
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab
set colorcolumn=120
set foldnestmax=10
set nofoldenable
set foldlevel=99
set foldmethod=syntax
autocmd FileType yaml,python BracelessEnable +highlight-cc +indent

" ================ Keybinds ========================={{{
" Mouse has keys?
set mouse=a

" Leader stuff
nnoremap <SPACE> <Nop>
let mapleader=" "

"ctrl+e to go to end of line in insert mode
inoremap <C-e> <C-o>$  

" Reload and open wim config
nmap co :vsp $MYVIMRC<CR>
nmap cr :source $MYVIMRC<CR>
nmap cq :source $MYVIMRC<CR>ZZ

" quicksave
nnoremap <c-s> :update<CR>
inoremap <c-s> <c-o>:update<CR>

" Orientation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"F keys Toggle key
map <silent><F2> :NvimTreeToggle<CR>
map <F3> :UndotreeToggle<cr>
map <silent><F4> <cmd>lua require('telescope.builtin').help_tags()<cr>

"keybind allows exiting IEx via Esc
if has("nvim")
  au TermOpen * tnoremap <Esc> <c-\><c-n>
endif

nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 5/4)<CR>
nnoremap <silent> <Leader>- :exe "resize " . (winheight(0) * 4/5)<CR>

" Testrunner
nnoremap <leader>tt :TestNearest<CR>
nnoremap <leader>tf :TestFile<CR>
nnoremap <leader>ts :TestSuite<CR>
nnoremap <leader>tl :TestLast<CR>
nnoremap <leader>tr :TestVisit<CR>

" In editor git stuff
nnoremap <leader>j :GitGutterNextHunk<CR>
nnoremap <leader>k :GitGutterPrevHunk<CR>
nnoremap <leader>p :GitGutterPreviewHunk<CR>
nmap <Leader>G <Plug>(git-messenger)

" clap search stuff
nnoremap <leader>s <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>b <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>H <cmd>lua require('telescope.builtin').oldfiles()<cr>
nnoremap <leader>F <cmd>lua require('telescope.builtin').find_files()<cr>

" fugitive git bindings
nnoremap <leader>ga :Git add %:p<CR><CR>
nnoremap <leader>gs :Git<CR>
nnoremap <leader>gc :Gcommit -v -q<CR>
nnoremap <leader>gt :Gcommit -v -q %:p<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>gw :Gwrite<CR><CR>
nnoremap <leader>gl :silent! Glog<CR>:bot copen<CR>
nnoremap <leader>gp :Ggrep<Space>
nnoremap <leader>gm :Gmove<Space>
nnoremap <leader>gb :Git branch<Space>
nnoremap <leader>go :Git checkout<Space>
nnoremap <leader>gps :Dispatch! git push<CR>
nnoremap <leader>gpl :Dispatch! git pull<CR>

" Smart coding
inoremap <silent><expr><c-space> coc#refresh()
nmap <leader>c :CocCommand<cr>
nmap <silent>gdd <Plug>(coc-definition)
nmap <silent>gdt <Plug>(coc-type-definition)
nmap <silent>gi <Plug>(coc-implementation)
nmap <silent>gr <Plug>(coc-references)
nmap <leader>rn <Plug>(coc-rename)
nmap <silent><leader>v  :<C-u>CocList diagnostics<cr>
nmap <silent><leader>e  :<C-u>CocList extensions<cr>
nmap <silent><leader>h <Plug>(coc-diagnostic-prev)
nmap <silent><leader>l <Plug>(coc-diagnostic-next)
xmap <leader>a <Plug>(coc-codeaction-selected)
nmap <leader>a <Plug>(coc-codeaction-selected)
nmap <leader>f <Plug>(coc-format):w<cr>:w<cr>

let g:vimspector_enable_mappings = 'HUMAN'

" Use K for show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" ================== Theme and Look ======================= {{{
" Colours and shit 
set termguicolors
set cursorline

let g:oceanic_next_terminal_bold = 1
let g:oceanic_next_terminal_italic = 1
syntax enable
colorscheme OceanicNext


hi CocHintSign guifg=#65737e
"hi Normal guibg=NONE ctermbg=NONE
"hi LineNr guibg=NONE ctermbg=NONE
"hi SignColumn guibg=NONE ctermbg=NONE
"hi EndOfBuffer guibg=NONE ctermbg=NONE

let g:edge_style = 'default'
let g:edge_enable_italic = 1
let g:edge_disable_italic_comment = 1
let g:edge_diagnostic_text_highlight = 1

hi SpellBad cterm=underline

let g:db_ui_show_database_icon = 1
let g:db_ui_use_nerd_fonts = 1
"
" coc-git handles icons
let g:gitgutter_signs = 0

" Markdown should be able to look prettier
let g:vim_markdown_fenced_languages = ['docker=Dockerfile', 'html', 'python', 'bash=sh', 'rust', 'javascript', 'js=javascript', 'css', 'cpp']
let g:semshi#mark_selected_nodes = 0

function MyCustomHighlights()
    hi semshiImported      ctermfg=214 guifg=#fabd22 cterm=bold gui=bold
endfunction
autocmd FileType python call MyCustomHighlights()

" ================== VimWiki ======================= {{{
let wiki = {}
let wiki.path = '~/vimwiki/'
let g:vimwiki_list = [wiki]
let g:vimwiki_list = [{'path': '~/vimwiki/',
      \ 'syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_global_ext = 0
let g:vimwiki_listsyms = '✗○◐●✓'

augroup vimwikigroup
    autocmd!
    " automatically update links on read diary
    autocmd BufRead,BufNewFile diary.wiki VimwikiDiaryGenerateLinks
augroup end


" ================== Linting ======================= {{{
let g:rustfmt_command = "rustfmt"
let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0


" DOGE - document generation
let g:doge_doc_standard_python = 'numpy'


" ================ Syntax and intelisense ================== {{{
set hidden
set cmdheight=2
set updatetime=300
set signcolumn=yes

let g:coc_global_extensions = [
      \ 'coc-snippets', 
      \ 'coc-highlight',
      \ 'coc-gitignore',
      \ 'coc-git', 
      \ 'coc-texlab',
      \ 'coc-erlang_ls',
      \ 'coc-db',
      \ 'coc-java', 
      \ 'coc-metals',
      \ 'coc-rust-analyzer', 
      \ 'coc-pyright', 
      \ 'coc-svelte', 
      \ 'coc-json', 
      \ 'coc-yaml', 
      \ 'coc-xml', 
      \ 'coc-tsserver', 
      \ 'coc-eslint', 
      \ 'coc-prettier', 
      \ 'coc-html', 
      \ 'coc-markdownlint']

autocmd BufNewFile,BufRead *.tex set filetype=tex
autocmd BufNewFile,BufRead *!.tex nmap <silent> <leader>h <Plug>(coc-diagnostic-prev)
autocmd BufNewFile,BufRead *!.tex nmap <silent> <leader>l <Plug>(coc-diagnostic-next)
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown


" Custom Commands
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" ============  Searching and Files ============ {{{
if executable('rg')
  set grepprg=rg\ --no-heading\ --vimgrep
  set grepformat=%f:%l:%c:%m
endif

let g:git_messenger_include_diff = "current"
let g:git_messenger_no_default_mappings = 1

let g:nvim_tree_width = 40 "30 by default
let g:nvim_tree_ignore = [] "empty by default
let g:nvim_tree_auto_open = 0 "0 by default, opens the tree when typing `vim $DIR` or `vim`
let g:nvim_tree_auto_close = 1 "0 by default, closes the tree when it's the last window
let g:nvim_tree_quit_on_open = 0 "0 by default, closes the tree when you open a file
let g:nvim_tree_follow = 1 "0 by default, this option allows the cursor to be updated when entering a buffer
let g:nvim_tree_indent_markers = 1 "0 by default, this option shows indent markers when folders are open
let g:nvim_tree_hide_dotfiles = 0 "0 by default, this option hides files and folders starting with a dot `.`
let g:nvim_tree_git_hl = 0 "0 by default, will enable file highlight for git attributes (can be used without the icons).
let g:nvim_tree_root_folder_modifier = ':~' "This is the default. See :help filename-modifiers for more options
let g:nvim_tree_tab_open = 1 "0 by default, will open the tree when entering a new tab and the tree was previously open
let g:nvim_tree_width_allow_resize  = 0 "0 by default, will not resize the tree when opening a file
let g:nvim_tree_disable_netrw = 0 "1 by default, disables netrw
let g:nvim_tree_hijack_netrw = 0 "1 by default, prevents netrw from automatically opening when opening directories (but lets you keep its other utilities)
let g:nvim_tree_show_icons = {
    \ 'git': 1,
    \ 'folders': 1,
    \ 'files': 1,
    \ }

" Decent wildmenu
set wildmenu
set wildmode=list:longest
set wildignore=.hg,.svn,*~,*.png,*.jpg,*.gif,*.settings,Thumbs.db,*.min.js,*.swp,publish/*,
      \intermediate/*,*.o,*.hi,Zend,vendor
      \*.pys

" ========== Testing configurations        ============ {{{
let test#strategy = 'dispatch'
au FileType qf wincmd J
let test#python#runner = 'nose'
" Runners available are 'pytest', 'nose', 'nose2', 'djangotest', 'djangonose' and Python's built-in 'unittest'

