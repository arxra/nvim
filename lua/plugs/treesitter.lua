require'nvim-treesitter.configs'.setup {
  ensure_installed = "maintained", -- one of "all", "maintained" (parsers with maintainers)
  highlight = {
    enable = true,
    disable = { "c"},  -- list of language that will be disabled
  }
}
