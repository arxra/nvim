local packer = require('packer')

packer.init {
  max_jobs = 5,
}

packer.startup(function(use)
  -- Depend on packer to keep it installed
  use { 'wbthomason/packer.nvim' }

  -- General stuff which are nice ---
  use { 'tpope/vim-dispatch' }
  use { 'tpope/vim-surround' }
  use { 'tpope/vim-dadbod' }
  use { 'kristijanhusak/vim-dadbod-ui' }
  use { 'tpope/vim-speeddating' }
  use { 'tpope/vim-repeat' }
  use { 'vimwiki/vimwiki' }
  use { 'janko/vim-test' }
  use { 'arxra/braceless.vim' } -- This is my custom fork since I needed async support.

  ----------------- Eye candy --------------
  use { 'mhartington/oceanic-next' }
  use { 'sainnhe/edge' }
  use { 'kyazdani42/nvim-web-devicons'}
  use { 'hardcoreplayers/oceanic-material' }
  use {
    'glepnir/galaxyline.nvim',
    config = function() require'statusline/evilline' end
  }
  use { 'kyazdani42/nvim-tree.lua' }

  ----------------- Fuzzy finder--------------
  use {
    'nvim-telescope/telescope.nvim',
    requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}},
    config={
      set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
      vimgrep_arguments = {
        'rg',
        '--color=never',
        '--no-heading',
        '--with-filename',
        '--line-number',
        '--column',
        '--smart-case'
      }
    }
  }

  ----------------- VCS ----------------------
  use { 'airblade/vim-gitgutter' }
  use { 'tpope/vim-fugitive' }
  use { 'rhysd/git-messenger.vim' }
  use { 'mbbill/undotree' }

  ----------------- Language packs -----------
  use { -- TreeSitter is really cool. Lets see how long it remains here before being merged into neovim
    'nvim-treesitter/nvim-treesitter',
    run=":TSUpdate",
    config = require'nvim-treesitter.configs'.setup {
      ensure_installed = "all", -- one of "all", "maintained" (parsers with maintainers)
      indent = {
        enable = true
      },
      highlight = {
        enable = true,
        disable = {"c"},  -- list of language that will be disabled
      }
    }
  }
  --use { 'plasticboy/vim-markdown' }
  --use { 'numirias/semshi', run=':UpdateRemotePlugins'}
  --use { 'rust-lang/rust.vim' }
  --use { 'arzg/vim-rust-syntax-ext' }
  use { 'mxw/vim-prolog' }
  use { 'cespare/vim-toml' }

  --- Auto complettion engines and plugs------
  use { 'neoclide/coc.nvim', branch = 'release'}
  use { 'honza/vim-snippets' }
  use { 'puremourning/vimspector' }
  use { 'kkoomen/vim-doge', run=':call doge#install()'}
end)

return packer
